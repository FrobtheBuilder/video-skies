"use strict";
var game = require("game");
var Stage = createjs.Stage;
var Ticker = createjs.Ticker;
Ticker.framerate = 60;
game.init(new Stage("screen"));
Ticker.addEventListener("tick", function (e) {
    game.update(e);
});
//# sourceMappingURL=index.js.map
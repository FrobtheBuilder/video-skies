module.exports = {
    entry: "./src/index.ts",
    devtool: "#inline-source-map",
    output: {
        path: __dirname + "/public",
        filename: "index.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader'
            }
        ]
    }
}

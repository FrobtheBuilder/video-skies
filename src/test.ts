import {Vector3} from "util/util"

console.log("testing 1 2 3 4 5 6 78... ")

console.log(new Vector3(2, 1, 1).add(new Vector3(1, 31, 1)))
console.log(new Vector3(2, 2, 2).multiply(3))
console.log(new Vector3(2, 2, 2).multiply(new Vector3(2, 1, 5)))
console.log(new Vector3(5, 5, 5).magnitude())

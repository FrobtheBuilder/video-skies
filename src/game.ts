"use strict"

import {LevelState} from "state/level-state"
import {SpacePosition} from "component/component"

let preloadState = new Phaser.State()

preloadState.preload = function() {
    this.game.forceSingleUpdate = true
    this.game.time.advancedTiming = true
    this.game.load.images([
        "great-enemy",
        "test"
    ],
    [
        "img/greatenemy.png",
        "img/test.png"
    ])
}

preloadState.create = function() {
    this.game.state.start("start")
}


let game = new Phaser.Game(800, 600, Phaser.CANVAS, "screen")

game.state.add("preload", preloadState)
game.state.add("start", new LevelState())

game.state.start("preload")
